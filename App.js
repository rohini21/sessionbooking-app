import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import BookSession from './screens/bookSession'
import TeacherProfile from './screens/teacherProfile'
import SucessScreen from './screens/SucessScreen'


const Stack = createStackNavigator()

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='BookSession'
        screenOptions={{
          gestureEnabled: true,
          headerStyle: {
            backgroundColor: '#e8e9f9'
          },
          headerTitleStyle: {
            fontWeight: 'bold'
          },
          headerTintColor: '#202d50',
          headerBackTitleVisible: false
        }}
        headerMode='float'>
        <Stack.Screen
          name='BookSession'
          component={BookSession}
          options={{ title: 'Book A Session' ,headerTitleStyle: { alignSelf: 'center', letterSpacing: 0.44}}}
          
        />
         <Stack.Screen
          name='TeacherProfile'
          component={TeacherProfile}
          options={({ route }) => ({
            title: route.params.item.name
          })}
        />
       <Stack.Screen
          name='SucessScreen'
          component={SucessScreen}
          options={{ headerShown: false }}
        />
     
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App