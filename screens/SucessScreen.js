import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Linking  } from 'react-native'
import Icon from "react-native-vector-icons/MaterialIcons";

function SucessScreen(props) {
  const { navigation } = props
  return (
    <View style={styles.container}>
    <View style={{display: "flex", flexDirection:"column", alignItems:"center"}}>
    <Icon size={48} color="#0fbb43" name='check-circle' />

      <Text style={[styles.text, {marginTop:20}]}>Booking Confirmed </Text>
      <Text  style={styles.text}>Successfully</Text>
      <Text style={{marginTop: 20}}>We have sent the meeting link in your email.</Text>
    </View>
    <View style={{ position: 'absolute',
    bottom:0,
    left:0,width:"100%"}}>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => Linking.openURL('mailto:support@example.com?subject= Session Booking&body=Hi John, Scheduled meeting on 26-11-2020 at 9.30 AM') }>
        <Text style={styles.buttonText}>Check Email</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.homeBtn}
        onPress={() => navigation.popToTop()}>
        <Text style={[styles.buttonText, {color:'#2026cb', fontWeight: '900'}]}>Go to Home</Text>
      </TouchableOpacity>
    </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ebebeb'
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
    display: "flex",
    justifyContent:"center"
  },
  homeBtn:{
      color: '#2026cb',
      borderRadius: 5,
      padding: 10,
      margin: 20,
      alignItems:"center",
      shadowOffset: { height: 1, width: 1 }, // IOS
      shadowRadius: 1, //IOS
      shadowColor: 'rgba(0,0,0, .1)',
  },
  buttonContainer: {
    backgroundColor: '#2026cb',
    borderRadius: 5,
    padding: 15,
    margin: 20,
    alignItems:"center",
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowRadius: 1, //IOS
    shadowColor: 'rgba(0,0,0, .1)',
    elevation: 2, // Android

  },
  buttonText: {
    fontSize: 20,
    textTransform:"uppercase",
    fontSize: 14,
    color: '#fff'
  }
})

export default SucessScreen
