import React, { Component } from 'react';
import { View, Button, TouchableOpacity, Text, FlatList, StyleSheet, LayoutAnimation, Platform, UIManager } from "react-native";
import { Colors } from './Colors/colors';
import Icon from "react-native-vector-icons/MaterialIcons";
import RNPickerSelect from 'react-native-picker-select';

export default class Accordian extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            expanded: false,
            costValue: 100
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    render() {
        const { costValue } = this.state
        return (
            <View>

                <TouchableOpacity style={styles.row} onPress={() => this.toggleExpand()}>
                    <Text style={[styles.title]}>{this.props.title}</Text>
                    <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={Colors.DARKGRAY} />
                </TouchableOpacity>
                <View style={styles.parentHr} />
                {
                    this.state.expanded &&
                    <View >
                            <View style={{width: "100%", borderRadius: 5,borderColor: '#e5eef2',
                borderWidth: 1, margin:15}}>
                            <View style={{display: "flex", flexDirection:'column', width: "100%", padding:15}}>
                            <Text style={styles.font} >Duration</Text>
                            <RNPickerSelect
                                style={pickerSelectStyles}
                                onValueChange={(value) => {console.log(value)
                                    this.setState({ costValue: 100*value })}}
                                value={'1'}
                                items={[
                                    { label: '1 hour', value: '1' },
                                    { label: '2 hour', value: '2' },
                                    { label: '3 hour', value: '3' },
                                ]}
                            />
                            </View>
                            <View style={{display: "flex", flexDirection:'column', width: "100%", padding:15}}>
                            <Text style={styles.font} >Type</Text>
                            <Text style={styles.option}>Zoom Video Conference Call</Text>
                            </View>
                            <View style={{display: "flex", flexDirection:'column', width: "100%", padding:15}}>
                            <Text style={styles.font} >Cost</Text>
                            <Text style={styles.option}>${this.state.costValue}</Text>
                            </View>
                            <Button
        title="Next"
        onPress={() => console.log('Simple Button pressed')}
      />
                            </View>
                    </View>
                }

            </View>
        )
    }

    toggleExpand = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ expanded: !this.state.expanded })
    }

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '100%',
        height: 54,
        alignItems: 'center',
        paddingLeft: 35,
        paddingRight: 35,
        fontSize: 12,
    },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
        color: Colors.DARKGRAY,
    },
    itemActive: {
        fontSize: 12,
        color: Colors.GREEN,
    },
    itemInActive: {
        fontSize: 12,
        color: Colors.DARKGRAY,
    },
    btnActive: {
        borderColor: Colors.GREEN,
    },
    btnInActive: {
        borderColor: Colors.DARKGRAY,
    },
    option:{
        color:"#384670",
        paddingTop: 8
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        paddingLeft: 25,
        paddingRight: 18,
        alignItems: 'center',
        backgroundColor: Colors.CGRAY,
    },
    childRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.GRAY,
    },
    parentHr: {
        height: 1,
        color: Colors.WHITE,
        width: '100%'
    },
    childHr: {
        height: 1,
        backgroundColor: Colors.LIGHTGRAY,
        width: '100%',
    },
    colorActive: {
        borderColor: Colors.GREEN,
    },
    colorInActive: {
        borderColor: Colors.DARKGRAY,
    }

});

const pickerSelectStyles = StyleSheet.create({

    inputAndroid: {
       
        color: '#384670',
    },
});