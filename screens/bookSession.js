import React from 'react'
import { StyleSheet, Dimensions, View, Image, Text, TouchableOpacity } from 'react-native'
import Icon from "react-native-vector-icons/MaterialIcons";

const character = {
  name: 'Book A Session',
  BookSession: 'Tatooine',
  species: 'human'
}

function BookSession(props) {
  const { navigation } = props;

  return (
    <View style={styles.container}>
      <Image
        style={styles.tinyLogo}
        source={{
          uri: 'https://cdn.zeplin.io/5fb261c2638bf16b96b6cc61/assets/3D4E1A88-B701-4FFB-965D-302C69D98EF5.png',
        }}
      />
      <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
        <Text style={[styles.text, { marginTop: 15, marginRight: 15 }]}>Captain America</Text>
        <Icon size={20} color="grey" name='share' />

      </View>

      <Text style={{ color: '#384670', fontSize: 14, marginTop: 5 }}>Career Consultant at Seawoods Uni</Text>
      <View
        style={{
          borderBottomColor: '#d7d7d7',
          borderBottomWidth: 1,
          width: "100%",
          marginTop: 15,
          marginBottom: 15
        }}
      />
        <View style={{display: "flex", flexDirection:"row", alignItems:"center", width: "100%", 
        marginLeft: 30}}>
      <Icon size={24} color="#5a6aef" name='supervised-user-circle' />
      <Text style={[styles.text, {marginLeft: 16}]}>Bio</Text>
      </View>
      <View style={{ borderWidth: 1, borderColor: "#d7d7d7", padding: 15, margin: 15, borderRadius: 10 }}>
        <Text style={{ color: '#202d50', fontSize: 14, fontWeight: "bold", letterSpacing: 0.44, textAlign: "left", paddingRight: 15 }}>“A free spirit, loves to paint, dance and sing aloud in the showers. The earth is a beautiful place and I believe in the power of nature. A free spirit, loves to paint, dance and sing aloud in the showers.
           The earth is a beautiful place and I believe in the power of nature.”</Text>
      </View>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => navigation.navigate('TeacherProfile', { item: character })}>
        <Text style={styles.buttonText}>Book A Session</Text>
      </TouchableOpacity>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold'
  },
  tinyLogo: {
    width: 90,
    height: 90,
  },
  buttonContainer: {
    backgroundColor: '#222',
    borderRadius: 5,
    padding: 15,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: Dimensions.get('window').width - 30,
    marginBottom: 20,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#2026cb',
    alignItems: "center",
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowRadius: 1, //IOS
    shadowColor: 'rgba(0,0,0, .1)',
    elevation: 2, // Android
  },
  buttonText: {
    fontSize: 12,
    textTransform: "uppercase",
    color: '#fff'
  }
})

export default BookSession