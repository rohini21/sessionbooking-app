import React, { Component } from 'react';
import { Button, Text, StyleSheet, View } from 'react-native';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';


export default class CalendarComp extends Component {
  _onPressButton() {
    console.log('You tapped the button!')
  }
  componentDidMount = () => {
    fetch('https://calendly.com/api/booking/event_types/CGGWQYMYXHQNHC2A/calendar/range?timezone=Asia%2FCalcutta&diagnostics=false&range_start=2020-11-21&range_end=2020-11-30&single_use_link_uuid=&embed_domain=&embed_type=Inline', {
       method: 'GET'
    })
    .then((response) => response.json())
    .then((responseJson) => {
       console.log("ro dta us",responseJson);
      
       let jsn = {}
 let array = responseJson.days.map((key1) => {

        let keyPro = {};
        jsn[key1.date] = {
            customStyles: {
                container: {
                  backgroundColor: '#fff',
                  borderWidth: 1,
                  borderColor: '#eee'
                },
                text: {
                  color: '#384670',
                  fontSize: 14
                }
              }
        }
        return ( keyPro);
     } );
     this.setState({
        markedDatesVal: jsn
     })
      
       console.log("hellodd", jsn)
    })
    .catch((error) => {
       console.error(error);
    });
 }
 
  constructor(props){
      super(props);
      this.state= {
        markedDatesVal:{}
      }

   
  }
  render() {
    return (
      <View style={styles.container}>

<CalendarList
  horizontal={true}
  // Enable paging on horizontal, default = false
  // Set custom calendarWidth.
  calendarWidth={350}
pastScrollRange={50}
// Max amount of months allowed to scroll to the future. Default = 50
futureScrollRange={50}
// Enable or disable scrolling of calendar list
scrollEnabled={true}
// Enable or disable vertical scroll indicator. Default = false
showScrollIndicator={true}
  markingType={'period'}
  theme={{
      
    backgroundColor: '#ffffff',
    calendarBackground: '#ffffff',
    textMonthFontSize:30,
    textSectionTitleColor: '#384670',
    textSectionTitleDisabledColor: '#d9e1e8',
    selectedDayBackgroundColor: 'red',
    selectedDayTextColor: 'green',
    todayTextColor: 'blue',
    dayTextColor: '#2d4150',
    textDisabledColor: '#d9e1e8',
    dotColor: '#00adf5',
    selectedDotColor: 'red',
    arrowColor: 'orange',
    disabledArrowColor: '#d9e1e8',
    monthTextColor: '#202d50',
    indicatorColor: 'blue',
    textDayFontFamily: 'monospace',
    textMonthFontFamily: 'monospace',
    textDayHeaderFontFamily: 'monospace',
    textDayFontWeight: '300',
    textMonthFontWeight: 'bold',
    textDayHeaderFontWeight: '300',
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16,
    arrowColor: 'white',
  'stylesheet.calendar.header': {
    week: {
      marginTop: 5,
      textTransform:"uppercase",
      flexDirection: 'row',
      justifyContent: 'space-between',
      
    }
  }
  }} 
  onDayPress={(day) => {console.log('selected day', day); day.color="red"}}
  markingType={'custom'}
  markedDates= {this.state.markedDatesVal}

/>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   
  },
  buttonContainer: {
    margin: 20
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});