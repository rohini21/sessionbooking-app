import React, { Component } from 'react';
import { View, Button, TouchableOpacity, Text, Dimensions, StyleSheet, LayoutAnimation, Platform, UIManager } from "react-native";
import { Colors } from '../Colors/colors';
import Icon from "react-native-vector-icons/MaterialIcons";
import RNPickerSelect from 'react-native-picker-select';
import { ScrollView } from 'react-native-gesture-handler';
export default class Details extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            expanded: false,
            costValue: 100
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    render() {
        const { costValue } = this.state
        return (
            <ScrollView>

                <View>

                    <TouchableOpacity style={styles.row} onPress={() => this.toggleExpand()}>

                        <Text style={[styles.title]}>{this.props.title}</Text>
                        <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={Colors.DARKGRAY} />
                    </TouchableOpacity>
                    <View style={styles.parentHr} />
                    {
                        this.state.expanded &&
                        <View >
                            <View style={{
                                width: Dimensions.get('window').width - 30, borderRadius: 5, borderColor: '#e5eef2',
                                borderWidth: 1, margin: 15
                            }}>
                                <View style={{ display: "flex", flexDirection: 'column', width: "100%", padding: 15 }}>
                                    <Text style={styles.font} >Duration</Text>
                                    <RNPickerSelect
                                        style={pickerSelectStyles}
                                        onValueChange={(value) => {
                                            console.log(value)
                                            this.setState({ costValue: 100 * value })
                                        }}
                                        value={'1'}
                                        items={[
                                            { label: '1 hour', value: '1' },
                                            { label: '2 hour', value: '2' },
                                            { label: '3 hour', value: '3' },
                                        ]}
                                    />
                                    <View
                                        style={{
                                            borderBottomColor: '#f2f2f2',
                                            borderBottomWidth: 1,
                                            width: "100%"
                                        }}
                                    />
                                </View>
                                <View style={{ display: "flex", flexDirection: 'column', width: "100%", padding: 15 }}>
                                    <Text style={styles.font} >Type</Text>
                                    <Text style={styles.option}>Zoom Video Conference Call</Text>
                                    <View
                                    style={{
                                        borderBottomColor: '#f2f2f2',
                                        borderBottomWidth: 1,
                                        width: "100%",
                                        marginTop:13
                                    }}
                                />
                                </View>
                             
                                <View style={{ display: "flex", flexDirection: 'column', width: "100%", padding: 15 }}>
                                    <Text style={styles.font} >Cost</Text>
                                    <Text style={styles.option}>${this.state.costValue}</Text>
                                </View>
                            </View>
                            <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => console.log('')}>
        <Text style={styles.buttonText}>Next</Text>
      </TouchableOpacity>
                        </View>
                    }

                </View>
            </ScrollView>

        )
    }

    toggleExpand = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ expanded: !this.state.expanded })
    }

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: '100%',
        height: 54,
        alignItems: 'center',
        paddingLeft: 35,
        paddingRight: 35,
        fontSize: 12,
    },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#202d50',
        letterSpacing: 0.44
    },
    itemActive: {
        fontSize: 12,
        color: Colors.GREEN,
    },
    itemInActive: {
        fontSize: 12,
        color: Colors.DARKGRAY,
    },
    btnActive: {
        borderColor: Colors.GREEN,
    },
    btnInActive: {
        borderColor: Colors.DARKGRAY,
    },
    option: {
        color: "#384670",
        paddingTop: 8
    },
    buttonContainer: {
        backgroundColor: '#222',
        borderRadius: 5,
        padding: 15,
        
        width: Dimensions.get('window').width - 30,
        marginBottom: 20,
        marginLeft: 15,
        marginRight: 15,
        backgroundColor: '#2026cb',
        alignItems: "center",
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowRadius: 1, //IOS
        shadowColor: 'rgba(0,0,0, .1)',
        elevation: 2, // Android
      },
      buttonText: {
        fontSize: 12,
        textTransform: "uppercase",
        color: '#fff'
      },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        paddingLeft: 25,
        paddingRight: 18,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    childRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.GRAY,
    },
    parentHr: {
        height: 1,
        color: Colors.WHITE,
        width: '100%'
    },
    childHr: {
        height: 1,
        backgroundColor: Colors.LIGHTGRAY,
        width: '100%',
    },
    colorActive: {
        borderColor: Colors.GREEN,
    },
    colorInActive: {
        borderColor: Colors.DARKGRAY,
    }

});

const pickerSelectStyles = StyleSheet.create({

    inputAndroid: {
        margin: 0,
        color: '#384670',
    },
});