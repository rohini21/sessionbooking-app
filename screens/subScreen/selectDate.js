import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, Text, FlatList, StyleSheet, LayoutAnimation, Platform, UIManager } from "react-native";
import { Colors } from '../Colors/colors';
import Icon from "react-native-vector-icons/MaterialIcons";
import CalendarComp from '../custom/calendar';
import RNPickerSelect from 'react-native-picker-select';
import { ScrollView } from 'react-native-gesture-handler';

export default class SelectDate extends React.Component {
  
     
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            expanded: false,
            costValue: 100
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    render() {
        const { costValue } = this.state
        return (
            <View>

                <TouchableOpacity style={styles.row} onPress={() => this.toggleExpand()}>
                    <Text style={[styles.title]}>{this.props.title}</Text>
                    <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={Colors.DARKGRAY} />
                </TouchableOpacity>
                <View style={styles.parentHr} />
                {
                    this.state.expanded &&
                    <ScrollView>
                        <CalendarComp/>
                    <View>
                        <View style={{ width: "100%", margin: 15, paddingBottom: 100 }}>
                            <View style={{ display: "flex", flexDirection: 'column', width: "100%", padding: 15 }}>
                                    <Text style={styles.font} >Time Zone</Text>
                                    <RNPickerSelect
                                        style={pickerSelectStyles}
                                        onValueChange={(value) => {
                                            console.log(value)
                                        }}
                                        disabled={true}
                                        value={'EST'}
                                        items={[
                                            { label: 'Indian Standard Time', value: 'IST' },
                                            { label: 'Eastern Standard Time', value: 'EST' },
                                            { label: ' Pacific Standard Time', value: 'PST' },
                                        ]}
                                    />
                                    <View
                                        style={{
                                            borderBottomColor: '#f2f2f2',
                                            borderBottomWidth: 1,
                                            width: "100%"
                                        }}
                                    />
                                </View>
                               
                        </View>
                      
                    </View>
                    </ScrollView>
                }

            </View>
        )
    }

    toggleExpand = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ expanded: !this.state.expanded })
    }

}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    font:{
        color:'#202d50',
        fontSize: 12,
        letterSpacing: 0.44
    },
    buttonContainer: {
        backgroundColor: '#222',
        borderRadius: 5,
        padding: 15,
        width: Dimensions.get('window').width - 30,
        marginBottom: 20,
      
        backgroundColor: '#2026cb',
        alignItems: "center",
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowRadius: 1, //IOS
        shadowColor: 'rgba(0,0,0, .1)',
        elevation: 2, // Android
      },
      buttonText: {
        fontSize: 12,
        textTransform: "uppercase",
        color: '#fff'
      },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#202d50',
        letterSpacing: 0.44
    },
    itemActive: {
        fontSize: 12,
        color: Colors.GREEN,
    },
    itemInActive: {
        fontSize: 12,
        color: Colors.DARKGRAY,
    },
    btnActive: {
        borderColor: Colors.GREEN,
    },
    btnInActive: {
        borderColor: Colors.DARKGRAY,
    },
    option: {
        color: "#384670",
        paddingTop: 8
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 56,
        paddingLeft: 25,
        paddingRight: 18,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    childRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: Colors.GRAY,
    },
    parentHr: {
        height: 1,
        color: Colors.WHITE,
        width: '100%'
    },
    childHr: {
        height: 1,
        backgroundColor: Colors.LIGHTGRAY,
        width: '100%',
    },
    colorActive: {
        borderColor: Colors.GREEN,
    },
    colorInActive: {
        borderColor: Colors.DARKGRAY,
    }

});

const pickerSelectStyles = StyleSheet.create({

    inputAndroid: {
        fontWeight: "bold",
        color: '#384670',
    },
});