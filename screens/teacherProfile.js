import React from 'react'
import { StyleSheet, Dimensions, View, Image, Text, TouchableOpacity } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import  Details from '../screens/subScreen/details';
import  SelectDate from '../screens/subScreen/selectDate';
import  SelectTime from '../screens/subScreen/selectTime';

function TeacherProfile(props) {
  const { route, navigation } = props
  const { item } = route.params


  return (
    <View style={styles.container}>

      <View style={{display:"flex", flexDirection:"row", marginTop: 14, alignItems:"center", paddingRight: 16, paddingLeft:16}}>
        <Image
        style={styles.tinyLogo}
        source={{
          uri: 'https://cdn.zeplin.io/5fb261c2638bf16b96b6cc61/assets/3D4E1A88-B701-4FFB-965D-302C69D98EF5.png',
        }}
      />
      <View style={{ display: "flex", flexDirection: "column" }}>
        <Text style={[styles.text, { marginTop: 15, marginRight: 15 }]}>Captain America</Text>
      <Text style={{ color: '#384670', fontSize: 14, marginTop: 5 }}>Career Consultant at Seawoods Uni</Text>
      </View>
      </View>
      <View
        style={{
          borderBottomColor: '#d7d7d7',
          borderBottomWidth: 1,
          width: "100%",
          marginTop: 15,
          marginBottom: 15
        }}
      />
      <View style= {styles.accContainer}>
<ScrollView styles={{height: 300}}>

      <Details
       title = {'Details'}/>
       <View
        style={{
          borderBottomColor: '#f2f2f2',
          borderBottomWidth: 1,
          width: "100%",
        }}
      />
        <SelectDate
       title = {'Select Date'}/>
       <View
        style={{
          borderBottomColor: '#f2f2f2',
          borderBottomWidth: 1,
          width: "100%"
        }}
      />
        <SelectTime
       title = {'Select Time'}/>
       <View
        style={{
          borderBottomColor: '#f2f2f2',
          borderBottomWidth: 1,
          width: "100%",
          marginTop: 15,
          marginBottom: 15
        }}
      />
       </ScrollView>
      </View>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => navigation.navigate('SucessScreen')}>
        <Text style={styles.buttonText}>Book Session Now</Text>
      </TouchableOpacity>
      
    </View>
    
  )
}
function renderAccordians(){
    const items = [];
    let menuItems =[
          { 
            title: 'Details', 
            data: [
              {key:'Chicken Biryani', value:false},
              {key:'Mutton Biryani', value:false},
              {key:'Prawns Biryani', value:false},
            ] 
          },
          { 
            title: 'Select Date',
            data: [
              {key:'Chicken Dominator', value:false},
              {key:'Peri Peri Chicken', value:false},
              {key:'Indie Tandoori Paneer', value:false},
              {key:'Veg Extraveganza', value:false}
            ]
          },
          { 
           title: 'Select Time',
           data: [
             {key:'Cocktail', value:false},
             {key:'Mocktail', value:false},
             {key:'Lemon Soda', value:false},
             {key:'Orange Soda', value:false}
            ]
          }
        ]
       
    for (let item of menuItems) {
        items.push(
            <Accordian 
                title = {item.title}
                data = {item.data}
                key={item.title}
            />
        );
    }
    return items;
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 20,
    backgroundColor: '#fff'
  },
  accContainer:{
    backgroundColor:'#fff'
  },
  tinyLogo: {
    width: 60,
    height: 60,
    marginRight: 15
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold'
  },
  card: {
    width: 350,
    height: 100,
    borderRadius: 10,
    backgroundColor: '#101010',
    margin: 10,
    padding: 10,
    alignItems: 'center'
  },
  cardText: {
    fontSize: 18,
    color: '#ffd700',
    marginBottom: 5
  },
  buttonContainer: {
    backgroundColor: '#222',
    borderRadius: 5,
    padding: 15,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: Dimensions.get('window').width - 30,
    marginBottom: 20,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#2026cb',
    alignItems: "center",
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowRadius: 1, //IOS
    shadowColor: 'rgba(0,0,0, .1)',
    elevation: 2, // Android
  },
  buttonText: {
    fontSize: 12,
    textTransform: "uppercase",
    color: '#fff'
  }
})

export default TeacherProfile